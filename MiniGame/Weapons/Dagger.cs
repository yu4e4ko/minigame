﻿using System;

namespace MiniGame.Weapons
{
    [Serializable]
    public class Dagger : Weapon
    {
        private const int DAMAGE = 16;
        private const int CRIT = 35;
        private const int MISS_CHANCE = 10;

        public Dagger()
        {
            Damage = DAMAGE;
            Crit = CRIT;
            MissChance = MISS_CHANCE;
        }

        public override string ToString()
        {
            return "dagger";
        }
    }
}
