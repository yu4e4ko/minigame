﻿using System;

namespace MiniGame.Weapons
{
    [Serializable]
    public class Sword : Weapon
    {
        private const int DAMAGE = 20;
        private const int CRIT = 15;
        private const int MISS_CHANCE = 15;

        public Sword()
        {
            Damage = DAMAGE;
            Crit = CRIT;
            MissChance = MISS_CHANCE;
        }

        public override string ToString()
        {
            return "sword";
        }
    }
}
