﻿
using System;

namespace MiniGame.Weapons
{
    [Serializable]
    public class Weapon
    {
        public int Damage { get; protected set; }
        public int Crit { get; protected set; }
        public int MissChance { get; protected set; }
    }
}
