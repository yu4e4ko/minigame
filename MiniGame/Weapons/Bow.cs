﻿using System;

namespace MiniGame.Weapons
{
    [Serializable]
    public class Bow : Weapon
    {
        private const int DAMAGE = 23;
        private const int CRIT = 5;
        private const int MISS_CHANCE = 30;

        public Bow()
        {
            Damage = DAMAGE;
            Crit = CRIT;
            MissChance = MISS_CHANCE;
        }

        public override string ToString()
        {
            return "bow";
        }
    }
}
