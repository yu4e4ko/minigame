﻿using System;
using System.Text;

namespace MiniGame
{
    public class GameManager
    {
        public int _round = 1;
        public string HitsManager(Player player1, Player player2)
        {
            var resultDamage = 0;

            var message = new StringBuilder();
            message.AppendLine();
            message.Append("It's round number ");
            message.Append(_round);
            message.AppendLine(". Fight!");

            message.Append("The first strike by ").Append(player1.Name).AppendLine(".");
            if (player1.PlaceForHit != player2.PlaceForDef)
            {
                if (Randomiser(player1.Weapon.MissChance) == false)
                {
                    //resultDamage = player1.Weapon.Damage;
                    resultDamage = GetDamage(player1.Weapon.Damage, player1.PlaceForHit);

                    if (Randomiser(player1.Weapon.Crit))
                    {
                        resultDamage *= 2;
                        message.Append("Critical strike! Double damage! ");
                    }
                    player2.Health -= resultDamage;
                    message.Append(ShowInfoAboutStrike(player1, player2, resultDamage));
                }
                else
                {
                    message.AppendLine("It's a miss! Ha-ha!");
                }
            }
            else
            {
                message.Append(player2.Name).Append(" successfully defended ").Append(player2.PlaceForDef).AppendLine(".");
            }
            
            message.Append("Second strike by ").Append(player2.Name).AppendLine(".");
            if (player2.PlaceForHit != player1.PlaceForDef)
            {
                if (Randomiser(player2.Weapon.MissChance) == false)
                {
                    //resultDamage = player2.Weapon.Damage;
                    resultDamage = GetDamage(player2.Weapon.Damage, player2.PlaceForHit);

                    if (Randomiser(player2.Weapon.Crit))
                    {
                        resultDamage *= 2;
                        message.Append("Critical strike! Double damage! ");
                    }
                    player1.Health -= resultDamage;
                    message.Append(ShowInfoAboutStrike(player2, player1, resultDamage));
                }
                else
                {
                    message.AppendLine("It's a miss! Ha-ha!");
                }
            }
            else
            {
                message.Append(player1.Name).Append(" successfully defended ").Append(player1.PlaceForDef).AppendLine(".");
            }

            message.Append("The current state of health of the players: ");
            message.Append(player1.Name);
            message.Append(" - ");
            message.Append(player1.Health);
            message.Append(", ");
            message.Append(player2.Name);
            message.Append(" - ");
            message.Append(player2.Health);
            message.Append(".");

            _round++;
            return message.ToString();
        }

        public int RoundCount
        {
            get { return _round; }
        }

        private bool Randomiser(int chance)
        {
            var probability = 0;
            var rnd = new Random();

            probability = rnd.Next(0, 100);

            if (probability <= chance)
            {
                return true;
            }
            return false;
        }

        private string ShowInfoAboutStrike (Player player1, Player player2, int damage)
        {
            StringBuilder s = new StringBuilder();
            
            s.Append(player1.Name).Append(" hit by ").Append(player1.Weapon).Append(" ");
            s.Append(player2.Name).Append(" in unprotected ").Append(player1.PlaceForHit).AppendLine(".");
            s.Append(player2.Name).Append(" lost ").Append(damage).AppendLine(" points of health.");

            return s.ToString();
        }

        private int GetDamage(int damage, PartsOfBody attackInPart)
        {
            switch (attackInPart)
            {
                case PartsOfBody.Head:
                    return (int)(damage * 1.2);
                case PartsOfBody.Foot:
                    return (int)(damage * 0.8);
            }

            return damage;
        }
    }
}
