﻿using System;

namespace MiniGame
{
    [Serializable]
    public class RoundResult
    {
        public Player ClientPlayer { get; set; }

        public Player ServerPlayer { get; set; }

        public string ServerMessage { get; set; }

        public bool GameIsEnd { get; set; }

        public Player Winner { get; set; }
    }
}
