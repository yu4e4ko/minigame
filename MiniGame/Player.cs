﻿using MiniGame.Weapons;
using System;

namespace MiniGame
{
    [Serializable]
    public class Player
    {
        public string Name { get; set; }
        public int Health { get; set; }
        public PartsOfBody PlaceForHit { get; set; }
        public PartsOfBody PlaceForDef { get; set; }
        public Weapon Weapon { get; set; }
    }
}
