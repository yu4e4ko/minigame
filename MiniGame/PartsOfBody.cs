﻿using System;

namespace MiniGame
{
    [Serializable]
    public enum PartsOfBody
    {
        Head,
        Body,
        Foot
    }
}
