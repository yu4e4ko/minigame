﻿using MiniGame;

namespace MiniGameUI
{
    internal class GameResolver
    {
        private readonly Player _currentPlayer;
        private readonly GameManager _gameManager;

        public GameResolver(Player player)
        {
            _currentPlayer = player;
            _gameManager = new GameManager();
        }

        public RoundResult ProcessRound(Player clientPlayer, bool isBot = false)
        {
            var resultMessage = _gameManager.HitsManager(_currentPlayer, clientPlayer);

            if (isBot == false)
            {
                //TODO
                //Common.WriteGameMessage(resultMessage);
            }

            Player winner = null;
            var isEndGame = false;

            if (_currentPlayer.Health < 1 && clientPlayer.Health < 1)
            {
                isEndGame = true;
            }
            else if(_currentPlayer.Health < 1 || clientPlayer.Health < 1)
            {
                isEndGame = true;

                if (_currentPlayer.Health < 1)
                {
                    winner = clientPlayer;
                }
                else if (clientPlayer.Health < 1)
                {
                    winner = _currentPlayer;
                }
            }

            var roundResult = new RoundResult
            {
                ServerMessage = resultMessage,
                GameIsEnd = isEndGame,
                ClientPlayer = clientPlayer,
                Winner = winner,
                ServerPlayer = _currentPlayer
            };

            return roundResult;
        }
    }
}
