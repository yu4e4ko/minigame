﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Navigation;
using Common;
using MiniGameUI.Helpers;
using MiniGameUI.Pages;

namespace MiniGameUI
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private const int PORT = 8001;
        internal static Server Server = new Server(PORT);
        internal static Client Client = new Client(PORT);

        public MainWindow()
        {
            InitializeComponent();

            ErrorHandler();

            this.ResizeMode = ResizeMode.CanMinimize;
            this.Title = "Minigame by Nastya & Yura";

            MainFrame.NavigationUIVisibility = NavigationUIVisibility.Hidden;
            WindowSwitcher.Switcher = this;
            WindowSwitcher.Switch(new StartUp());
        }

        public void Navigate(Page nextPage)
        {
            MainFrame.Navigate(nextPage);
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            Server.Dispose();
            Client.Dispose();
        }

        private void ErrorHandler()
        {
            Dispatcher.UnhandledException += (sender, args) =>
            {
                args.Handled = true;
                MessageBox.Show(args.Exception.Message, "Application error");
                WindowSwitcher.Switch(new StartUp());
            };
        }
    }
}
