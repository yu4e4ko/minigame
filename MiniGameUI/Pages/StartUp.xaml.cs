﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using MiniGame;
using MiniGame.Weapons;
using MiniGameUI.Helpers;

namespace MiniGameUI.Pages
{
    /// <summary>
    /// Interaction logic for StartUp.xaml
    /// </summary>
    public partial class StartUp : Page
    {
        private readonly string _myIpAddress;

        public StartUp()
        {
            InitializeComponent();

            clientGrid.Visibility = Visibility.Hidden;
            ChangeActiveStatus(true);

            var ipHostInfo = Dns.Resolve(Dns.GetHostName());
            _myIpAddress = ipHostInfo.AddressList[0].ToString();

            radioServer.IsChecked = true;
            radioSword.IsChecked = true;
            txtServerIp.Text = IpSaver.GetLastIp();
        }

        private void radioServer_Checked(object sender, RoutedEventArgs e)
        {
            var message = $"Server ip is '{_myIpAddress}'";
            txtBlockServerIP.Text = message;

            clientGrid.Visibility = Visibility.Hidden;
            serverGrid.Visibility = Visibility.Visible;
        }

        private void radioClient_Checked(object sender, RoutedEventArgs e)
        {
            clientGrid.Visibility = Visibility.Visible;
            serverGrid.Visibility = Visibility.Hidden;
        }

        private async void btnStart_Click(object sender, RoutedEventArgs e)
        {
            if (radioClient.IsChecked == true)
            {
                await RunClient();
            }
            else
            {
                await RunServer();
            }
        }

        private async Task RunServer()
        {
            var name = txtName.Text;

            if (CheckName(name) == false)
            {
                return;
            }

            var player = CreatePlayer();
            ChangeActiveStatus(false, "Waiting for client...");

            if (await MainWindow.Server.RunServerAsync(_myIpAddress))
            {
                WindowSwitcher.Switch(new FightPage(player, true));
            }
            else
            {
                ChangeActiveStatus(true);
            }
        }

        private async Task RunClient()
        {
            var ipAddress = txtServerIp.Text;
            var name = txtName.Text;

            if (CheckServerIp(ipAddress) == false)
            {
                MessageBox.Show("Invalid server ip!", "Error");
                return;
            }

            if (CheckName(name) == false)
            {
                return;
            }

            IpSaver.SaveLastIp(ipAddress);

            var player = CreatePlayer();
            ChangeActiveStatus(false, "Connecting...");

            if (await MainWindow.Client.ConnectAsync(ipAddress))
            {
                WindowSwitcher.Switch(new FightPage(player, false));
            }
            else
            {
                ChangeActiveStatus(true);
            }
        }

        private bool CheckName(string name)
        {
            if (CheckEmptyName(name) == false)
            {
                MessageBox.Show("Enter you name please", "Error");
                return false;
            }

            if (CheckNameLength(name) == false)
            {
                MessageBox.Show("Name should be less then 100 symbols", "Error");
                return false;
            }

            return true;
        }

        private bool CheckEmptyName(string name)
        {
            return string.IsNullOrEmpty(name) == false;
        }

        private bool CheckNameLength(string name)
        {
            return name.Length < 100;
        }

        private bool CheckServerIp(string ip)
        {
            Regex regex = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            Match match = regex.Match(ip);

            return match.Success;
        }

        private void ChangeActiveStatus(bool enabled, string message = "")
        {
            txtName.IsEnabled = enabled;
            radioClient.IsEnabled = enabled;
            radioServer.IsEnabled = enabled;
            btnStart.IsEnabled = enabled;

            radioBow.IsEnabled = enabled;
            radioDagger.IsEnabled = enabled;
            radioSword.IsEnabled = enabled;

            if (enabled == false)
            {
                textMessage.Visibility = Visibility.Visible;
                textMessage.Text = message;
            }
            else
            {
                textMessage.Visibility = Visibility.Hidden;
                textMessage.Text = string.Empty;
            }
        }

        private Player CreatePlayer()
        {
            var player = new Player
            {
                Name = txtName.Text,
                Health = 100,
                Weapon = GetWeapon()
            };

            return player;
        }

        private Weapon GetWeapon()
        {
            if (radioSword.IsChecked == true)
            {
                return new Sword();
            }
            else if (radioDagger.IsChecked == true)
            {
                return new Dagger();
            }

            return new Bow();
        }
    }
}
