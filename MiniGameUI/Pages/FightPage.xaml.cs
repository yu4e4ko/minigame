﻿using System.Windows;
using System.Windows.Controls;
using MiniGame;
using MiniGameUI.Helpers;

namespace MiniGameUI.Pages
{
    /// <summary>
    /// Interaction logic for FightPage.xaml
    /// </summary>
    public partial class FightPage : Page
    {
        private Player _player;
        private readonly bool _isServer;
        private GameResolver _gameResolver;

        public FightPage(Player player, bool isServer)
        {
            InitializeComponent();

            _player = player;
            _isServer = isServer;

            InitFight();
        }

        private void InitFight()
        {
            radioDefHead.IsChecked = true;
            radioAttackHead.IsChecked = true;
            gridOpponent.Visibility = Visibility.Hidden;

            _gameResolver = new GameResolver(_player);

            txtHealth.Text = _player.Health.ToString();
            txtPlayerName.Text = _player.Name;
            txtWeapon.Text = _player.Weapon.ToString();

            SetActiveStatus(true);
        }

        private async void btnFight_Click(object sender, System.Windows.RoutedEventArgs e)
        {
            _player.PlaceForDef = GetDefPartOfBody();
            _player.PlaceForHit = GetAttackPartOfBody();

            RoundResult roundResult;

            if (_isServer)
            {
                SetActiveStatus(false, "Waiting for client...");
                var opponent = await MainWindow.Server.ReceivePlayerAsync();
                SetActiveStatus(true);

                roundResult = _gameResolver.ProcessRound(opponent);
                await MainWindow.Server.SendRoundResultAsync(roundResult);

                SetOpponentInfo(roundResult.ClientPlayer);
            }
            else
            {
                SetActiveStatus(false, "Waiting for server...");
                roundResult = await MainWindow.Client.SendPlayerInfoAsync(_player);
                SetActiveStatus(true);

                _player = roundResult.ClientPlayer;
                SetOpponentInfo(roundResult.ServerPlayer);
            }

            UpdatePlayerHealth();
            MessageBox.Show(roundResult.ServerMessage, "Round result");

            if (roundResult.GameIsEnd)
            {
                ShowGameOver(roundResult);
            }
        }

        private PartsOfBody GetDefPartOfBody()
        {
            if (radioDefHead.IsChecked == true)
            {
                return PartsOfBody.Head;
            }
            else if (radioDefBody.IsChecked == true)
            {
                return PartsOfBody.Body;
            }

            return PartsOfBody.Foot;
        }

        private PartsOfBody GetAttackPartOfBody()
        {
            if (radioAttackHead.IsChecked == true)
            {
                return PartsOfBody.Head;
            }
            else if (radioAttackBody.IsChecked == true)
            {
                return PartsOfBody.Body;
            }

            return PartsOfBody.Foot;
        }

        private void SetActiveStatus(bool enabled, string message = "")
        {
            gridPlayerAttack.IsEnabled = enabled;
            gridPlayerDef.IsEnabled = enabled;
            btnFight.IsEnabled = enabled;

            if (enabled == false)
            {
                textMessage.Visibility = Visibility.Visible;
                textMessage.Text = message;
            }
            else
            {
                textMessage.Visibility = Visibility.Hidden;
                textMessage.Text = string.Empty;
            }
        }

        private void UpdatePlayerHealth()
        {
            txtHealth.Text = _player.Health.ToString();
        }

        private void SetOpponentInfo(Player player)
        {
            if (gridOpponent.Visibility == Visibility.Hidden)
            {
                gridOpponent.Visibility = Visibility.Visible;
            }
            
            txtEnemyHealth.Text = player.Health.ToString();
            txtEnemyName.Text = player.Name;
            txtEnemyWeapon.Text = player.Weapon.ToString();
        }

        private void ShowGameOver(RoundResult roudResult)
        {
            var message = roudResult.Winner != null
                ? $"Game over! Winner is {roudResult.Winner.Name}!"
                : "Game over! Draw...";

            MessageBox.Show(message);
        }
    }
}
