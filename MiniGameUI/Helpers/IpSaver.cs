﻿using System;
using System.IO;

namespace MiniGameUI.Helpers
{
    internal class IpSaver
    {
        private const string FILE_NAME = "lastIp.txt";

        public static void SaveLastIp(string ipAddress)
        {
            if (File.Exists(FILE_NAME) == false)
            {
                File.Create(FILE_NAME);
            }

            using (var streamWriter = new StreamWriter(FILE_NAME))
            {
                streamWriter.WriteLine(ipAddress);
            }
        }

        public static string GetLastIp()
        {
            if (File.Exists(FILE_NAME) == false)
            {
                return string.Empty;
            }

            using (var streamReader = new StreamReader(FILE_NAME))
            {
                var lastIp = streamReader.ReadLine();

                if (string.IsNullOrEmpty(lastIp))
                {
                    return string.Empty;
                }

                return lastIp;
            }
        }
    }
}
