﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Common;
using MiniGame;

namespace MiniGameUI.Helpers
{
    internal static class ServerExtensions
    {
        public static Task<bool> RunServerAsync(this Server @this, string ipAddress)
        {
            var task = Task.Run(
                () =>
                {
                    try
                    {
                        return @this.RunServer(ipAddress);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error! {ex.Message}", ex);
                    }
                });

            return task;
        }

        public static Task<Player> ReceivePlayerAsync(this Server @this)
        {
            var task = Task.Run(
                () =>
                {
                    try
                    {
                        return @this.ReceivePlayer();
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error! {ex.Message}", ex);
                    }
                });

            return task;
        }

        public static Task SendRoundResultAsync(this Server @this, RoundResult roundResult)
        {
            var task = Task.Run(
                () =>
                {
                    try
                    {
                        @this.SendRoundResult(roundResult);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error! {ex.Message}", ex);
                    }
                });

            return task;
        }
    }
}
