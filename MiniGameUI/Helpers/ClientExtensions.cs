﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Common;
using MiniGame;

namespace MiniGameUI.Helpers
{
    internal static class ClientExtensions
    {
        public static Task<bool> ConnectAsync(this Client @this, string ipAddress)
        {
            var task = Task.Run(
                () =>
                {
                    try
                    {
                        return @this.Connect(ipAddress);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error! {ex.Message}", ex);
                    }
                });

            return task;
        }

        public static Task<RoundResult> SendPlayerInfoAsync(this Client @this, Player player)
        {
            var task = Task.Run(
                () =>
                {
                    try
                    {
                        return @this.SendPlayerInfo(player);
                    }
                    catch (Exception ex)
                    {
                        throw new Exception($"Error! {ex.Message}", ex);
                    }
                });

            return task;
        }
    }
}
