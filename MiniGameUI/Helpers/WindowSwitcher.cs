﻿using System.Windows.Controls;

namespace MiniGameUI.Helpers
{
    internal class WindowSwitcher
    {
        public static MainWindow Switcher { get; set; }

        public static void Switch(Page nextPage)
        {
            Switcher.Navigate(nextPage);
        }
    }
}
