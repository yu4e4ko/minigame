﻿using MiniGame;

namespace ConsoleUI
{
    internal interface IPlayerReceiver
    {
        Player ReceivePlayer();

        void SendRoundResult(RoundResult roundResult);
    }
}
