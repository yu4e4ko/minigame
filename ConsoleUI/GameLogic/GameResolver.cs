﻿using System;
using MiniGame;

namespace ConsoleUI.GameLogic
{
    internal class GameResolver
    {
        private readonly Player _currentPlayer;
        private readonly GameManager _gameManager;

        public GameResolver(Player player)
        {
            _currentPlayer = player;
            _gameManager = new GameManager();
        }

        public RoundResult ProcessRound(Player clientPlayer, bool isBot = false)
        {
            if (isBot == false)
            {
                SetPlayerState();
            }

            var resultMessage = _gameManager.HitsManager(_currentPlayer, clientPlayer);

            if (isBot == false)
            {
                Common.WriteGameMessage(resultMessage);
            }

            Player winner = null;
            var isEndGame = false;

            if (_currentPlayer.Health < 1 && clientPlayer.Health < 1)
            {
                isEndGame = true;
            }
            else if(_currentPlayer.Health < 1 || clientPlayer.Health < 1)
            {
                isEndGame = true;

                if (_currentPlayer.Health < 1)
                {
                    winner = clientPlayer;
                }
                else if (clientPlayer.Health < 1)
                {
                    winner = _currentPlayer;
                }
            }

            var roundResult = new RoundResult
            {
                ServerMessage = resultMessage,
                GameIsEnd = isEndGame,

                // I am apponent for client
                ClientPlayer = clientPlayer,
                Winner = winner,
                ServerPlayer = _currentPlayer
            };

            return roundResult;
        }

        private void SetPlayerState()
        {
            Common.WriteLine("What attack?");
            _currentPlayer.PlaceForHit = Common.GetPartOfBody("attack");
            Common.WriteLine($"Attack is {_currentPlayer.PlaceForHit}");
            Console.WriteLine("");

            Common.WriteLine("What deff?");
            _currentPlayer.PlaceForDef = Common.GetPartOfBody("deff");
            Common.WriteLine($"Deff is {_currentPlayer.PlaceForDef}");
            Console.WriteLine("");
        }
    }
}
