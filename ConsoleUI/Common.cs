﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using MiniGame;
using MiniGame.Weapons;

namespace ConsoleUI
{
    internal class Common
    {
        public static byte[] ObjectToByteArray<T>(T someObject)
        {
            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, someObject);
                return memoryStream.ToArray();
            }

            throw new Exception("Error! Player -> byte array.");
        }

        public static T ByteArrayToObject<T>(byte[] bytes, int length)
        {
            var memoryStream = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();

            T resultObject;

            try
            {
                memoryStream.Write(bytes, 0, length);
                memoryStream.Seek(0, SeekOrigin.Begin);

                resultObject = (T) binaryFormatter.Deserialize(memoryStream);
            }
            catch (Exception ex)
            {
                throw new Exception("Error! bytes -> PlayerResult. ", ex);
            }

            return resultObject;
        }

        public static void WriteLine(string message)
        {
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine($"> {message}");
            Console.WriteLine();
            Console.ResetColor();
        }

        public static PartsOfBody GetPartOfBody(string action)
        {
            WriteLine($"Choose part of body for {action}:\n'1' -> Head\n'2' -> Body\n'Any key' -> Foots");

            var enterd = ReadLine();

            switch (enterd)
            {
                case "1":
                    return PartsOfBody.Head;
                case "2":
                    return PartsOfBody.Body;
                default:
                    return PartsOfBody.Foot;
            }
        }

        public static Weapon ChooseWeapon()
        {
            WriteLine("Choose your weapon:\n'1' - Bow;\n'2' - Dagger;\n'Any key' -> Sword");

            var enterd = ReadLine();

            switch (enterd)
            {
                case "1":
                    return new Bow();
                case "2":
                    return new Dagger();
                default:
                    return new Sword();
            }
        }

        public static void WriteGameMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("******************************");
            Console.WriteLine($"\t{message}");
            Console.WriteLine("******************************\n");
            Console.ResetColor();
        }

        public static void DebugWriteLine(string message)
        {
            Console.WriteLine($"> DEBUG: {message}");
        }

        public static string ReadLine()
        {
            Console.ForegroundColor = ConsoleColor.Green;
            var line = Console.ReadLine();
            Console.ResetColor();
            return line;
        }
    }
}
