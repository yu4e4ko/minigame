﻿using System;
using System.Net.Sockets;
using MiniGame;

namespace ConsoleUI.ServerClient
{
    internal class Client : IDisposable
    {
        private TcpClient _tcpClient;

        public void Connect(string ipAddress)
        {
            try
            {
                _tcpClient = new TcpClient();
                Console.WriteLine("Connecting.....");

                _tcpClient.Connect(ipAddress, 8001);
                // use the ipaddress as in the server program
                Console.WriteLine("Connected");
            }

            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }

        public RoundResult SendPlayerInfo(Player player)
        {
            var playeerBytes = Common.ObjectToByteArray(player);

            var stream = _tcpClient.GetStream();
            stream.Write(playeerBytes, 0, playeerBytes.Length);

            Console.WriteLine("Player sent");

            var responseMaxLenght = 2000;
            var response = new byte[responseMaxLenght];
            var responseLengh = stream.Read(response, 0, responseMaxLenght);

            var roundResult = Common.ByteArrayToObject<RoundResult>(response, responseLengh);

            return roundResult;
        }

        public void Dispose()
        {
            _tcpClient.Close();
        }
    }
}
