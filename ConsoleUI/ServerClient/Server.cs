﻿using System;
using System.Net;
using System.Net.Sockets;
using MiniGame;

namespace ConsoleUI.ServerClient
{
    internal class Server : IDisposable, IPlayerReceiver
    {
        private Socket _soket;
        private TcpListener _tcpListener;

        public bool RunServer(string ipAddress)
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse(ipAddress);
                _tcpListener = new TcpListener(ipAd, 8001);

                /* Start Listeneting at the specified port */
                _tcpListener.Start();

                Console.WriteLine("\n**********************************");
                Console.WriteLine("The server is running at port 8001...");
                Console.WriteLine("The local End point is " + _tcpListener.LocalEndpoint);
                Console.WriteLine("Waiting for a connection.....");
                Console.WriteLine("**********************************\n");

                _soket = _tcpListener.AcceptSocket();

                Console.WriteLine("\n**********************************");
                Console.WriteLine("Connection accepted from " + _soket.RemoteEndPoint);
                Console.WriteLine("**********************************\n");

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
                return false;
            }
        }

        public Player ReceivePlayer()
        {
            var playerBytesMaxLength = 2000;
            var playerBytes = new byte[playerBytesMaxLength];

            var playerBytesLength = _soket.Receive(playerBytes);

            var player = Common.ByteArrayToObject<Player>(playerBytes, playerBytesLength);
            return player;
        }

        public void SendRoundResult(RoundResult roundResult)
        {
            var roundResultBytes = Common.ObjectToByteArray(roundResult);
            _soket.Send(roundResultBytes);
        }

        public void Dispose()
        {
            _soket.Close();
            _tcpListener.Stop();
        }
    }
}
