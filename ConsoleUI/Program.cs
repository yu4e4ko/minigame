﻿using System;
using System.Net;
using System.Text.RegularExpressions;
using ConsoleUI.Bots;
using ConsoleUI.GameLogic;
using ConsoleUI.ServerClient;
using MiniGame;
using MiniGame.Weapons;

namespace ConsoleUI
{
    class Program
    {
        static void Main(string[] args)
        {
            // BOTS TESTS
            TestWeapon(new Sword(), new Dagger());
            TestWeapon(new Sword(), new Bow());
            TestWeapon(new Dagger(), new Bow());

            // Play game
            //PlayGame();
        }

        static void TestWeapon(Weapon firstBotWeapon, Weapon secondBotWeapon)
        {
            var botVsBot = new BotVsBot();
            var result = botVsBot.PlayBotVsBot(100000, firstBotWeapon, secondBotWeapon);
            Console.WriteLine(result);
            Console.WriteLine("\n\n=============================================\n\n");
        }

        static void PlayGame()
        {
            var ipHostInfo = Dns.Resolve(Dns.GetHostName());
            var ipAddress = ipHostInfo.AddressList[0].ToString();

            Common.WriteLine("Hi player!");
            var player = SetupPlayer();
            var gameResovler = new GameResolver(player);
            Player winner = null;

            if (IsPlayOnline())
            {
              if (IsServer())
              {
                Common.WriteLine($"Your ip address: {ipAddress}");
                winner = StartServer(ipAddress, gameResovler);
              }
              else
              {
                var enteredIp = EnterServerIp();
                Console.WriteLine("\n");
                winner = StartClient(enteredIp, player);
              }
            }
            else
            {
              var bot = new SimpleBot("Simple bot");
              Common.WriteLine($"You are playing with {bot.Name}\n");
              winner = StartPlayWithBot(bot, gameResovler);
            }

            if (winner != null)
            {
              Common.WriteLine($"Game over! {winner.Name} is win!");
            }
            else
            {

              Common.WriteLine("Game over! DRAW");
            }

            Console.ReadKey();
        }

        #region main

        static bool IsPlayOnline()
        {
            Common.WriteLine("Choose game mode:\n'1' - Online\n'Any key' - Play with bot");
            var enterd = Common.ReadLine();

            return enterd == "1";
        }

        static bool IsServer()
        {
            Common.WriteLine("You are server? Y/N");
            var key = Common.ReadLine();

            if (key.ToLower() == "y")
            {
                Common.WriteLine("Ok. You are server!");
                return true;
            }

            Common.WriteLine("Ok. You are client!");
            return false;
        }

        static Player SetupPlayer()
        {
            Common.WriteLine("Enter player name");
            var playerName = Common.ReadLine();
            var weapon = Common.ChooseWeapon();

            var player = new Player
            {
                Name = playerName,
                Health = 100,
                Weapon = weapon
            };

            Common.WriteLine($"Your weapon is {weapon}");

            return player;
        }

        static string EnterServerIp()
        {
            Common.WriteLine("Enter ip address of server");
            var enteredIp = Common.ReadLine();

            Regex regex = new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}");
            Match match = regex.Match(enteredIp);

            while (match.Success == false)
            {
                Common.WriteLine("Invalid ip!");
                Common.WriteLine("Enter ip address of server");
                enteredIp = Common.ReadLine();
                match = regex.Match(enteredIp);
            }

            return enteredIp;
        }

        static Player StartServer(string ipAddress, GameResolver gameResolver)
        {
            using (var server = new Server())
            {
                server.RunServer(ipAddress);
                return ProcessRound(server, gameResolver, "client");
            }
        }

        static Player StartPlayWithBot(IBot bot, GameResolver gameResolver)
        {
            return ProcessRound(bot, gameResolver, bot.Name);
        }

        static Player ProcessRound(IPlayerReceiver playerReceiver, GameResolver gameResolver, string opponentName)
        {
            var roundResult = new RoundResult();

            while (roundResult.GameIsEnd == false)
            {
                Common.WriteLine($"Waiting of {opponentName} response...\n");
                var player = playerReceiver.ReceivePlayer();
                roundResult = gameResolver.ProcessRound(player);
                playerReceiver.SendRoundResult(roundResult);
            }

            return roundResult.Winner;
        }

        static Player StartClient(string ipAddress, Player player)
        {
            using (var client = new Client())
            {
                client.Connect(ipAddress);

                RoundResult result = new RoundResult();

                while (result.GameIsEnd == false)
                {
                    Common.WriteLine("What attack?");
                    player.PlaceForHit = Common.GetPartOfBody("attack");
                    Common.WriteLine($"Attack is {player.PlaceForHit}");
                    Console.WriteLine("");

                    Common.WriteLine("What deff?");
                    player.PlaceForDef = Common.GetPartOfBody("deff");
                    Common.WriteLine($"Deff is {player.PlaceForDef}");
                    Console.WriteLine("");

                    result = client.SendPlayerInfo(player);
                    Common.WriteLine("Waiting of server response...");
                    Common.WriteGameMessage(result.ServerMessage);

                    player = result.ClientPlayer;
                }

                return result.Winner;
            }
        }

        #endregion
    }
}
