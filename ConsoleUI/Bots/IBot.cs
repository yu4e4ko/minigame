﻿namespace ConsoleUI.Bots
{
    internal interface IBot : IPlayerReceiver
    {
        string Name { get; set; }
    }
}
