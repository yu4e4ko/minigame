﻿using System;
using MiniGame;
using MiniGame.Weapons;

namespace ConsoleUI.Bots
{
    internal class SimpleBot : IBot
    {
        //private const string BOT_NAME = "Simple bot";
        private const int MAX_PLAYER_HP = 100;

        private readonly Player _player;
        private readonly Random _random;

        public SimpleBot(string name) : this(name, MAX_PLAYER_HP)
        {
        }

        public SimpleBot(string name, int maxPlayerHP, Weapon weapon = null)
        {
            Name = name;
            _random = new Random();

            _player = new Player
            {
                Name = this.Name,
                Health = maxPlayerHP,
                Weapon = weapon ?? GetRandomWeapon()
            };
        }

        public string Name { get; set; }

        public Player ReceivePlayer()
        {
            _player.PlaceForHit = GetRandomPathOfBody();
            _player.PlaceForDef = GetRandomPathOfBody();

            return _player;
        }

        public void SendRoundResult(RoundResult roundResult)
        {
            // Not nead
        }

        private PartsOfBody GetRandomPathOfBody()
        {
            var randomNumber = _random.Next(1, 4);

            switch (randomNumber)
            {
                case 1:
                    return PartsOfBody.Head;
                case 2:
                    return PartsOfBody.Body;
                default:
                    return PartsOfBody.Foot;
            }
        }

        private Weapon GetRandomWeapon()
        {
            var random = new Random();
            var randomNumber = random.Next(1, 4);

            switch (randomNumber)
            {
                case 2:
                    return new Bow();
                case 3:
                    return new Dagger();
                default:
                    return new Sword();
            }
        }
    }
}
