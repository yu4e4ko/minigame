﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsoleUI.GameLogic;
using MiniGame;
using MiniGame.Weapons;

namespace ConsoleUI.Bots
{
    internal class BotVsBot
    {
        private int _bot1WinsCount;
        private int _bot2WinsCount;
        private int _drawCount;
        private Dictionary<Type, int> _weaponStatWins;
        private Dictionary<Type, int> _weaponStatUsed; 

        public string PlayBotVsBot(int fightCount,
          Weapon firstBotWeapon = null,
          Weapon secondBotWeapon = null)
        {
            InitStats();

            SimpleBot bot1;
            SimpleBot bot2;
            GameResolver gameResolver;

            for (var i = 0; i < fightCount; i++)
            {
                bot1 = new SimpleBot("bot1", 100, firstBotWeapon);
                bot2 = new SimpleBot("bot2", 100, secondBotWeapon);
                AddStatFromBot(bot1);
                AddStatFromBot(bot2);

                gameResolver = new GameResolver(bot1.ReceivePlayer());
                
                RoundResult roundResult = new RoundResult();

                while (roundResult.GameIsEnd == false)
                {
                    roundResult = gameResolver.ProcessRound(bot2.ReceivePlayer(), true);
                }

                if (roundResult.Winner == null)
                {
                    _drawCount++;
                }
                else if (roundResult.Winner.Name == bot1.Name)
                {
                    _bot1WinsCount++;

                    if (bot1.ReceivePlayer().Weapon.GetType() != bot2.ReceivePlayer().Weapon.GetType())
                    {
                        AddWinWeaponStat(bot1);
                    }
                }
                else
                {
                    _bot2WinsCount++;

                    if (bot1.ReceivePlayer().Weapon.GetType() != bot2.ReceivePlayer().Weapon.GetType())
                    {
                        AddWinWeaponStat(bot2);
                    }
                }
            }

            return GetReport();
        }

        private string GetReport()
        {
            var stringBuilder = new StringBuilder();

            stringBuilder.AppendLine($"Draw count: {_drawCount}");
            stringBuilder.AppendLine($"Bot 1 wins count: {_bot1WinsCount}");
            stringBuilder.AppendLine($"Bot 2 wins count: {_bot2WinsCount}");

            stringBuilder.AppendLine("*** USED ****");
            foreach (var keyPair in _weaponStatUsed)
            {
                stringBuilder.AppendLine($"{keyPair.Key.Name}: {keyPair.Value}");
            }

            stringBuilder.AppendLine("***").AppendLine();

            stringBuilder.AppendLine("*** WINS ****");
            foreach (var keyPair in _weaponStatWins)
            {
                stringBuilder.AppendLine($"{keyPair.Key.Name}: {keyPair.Value}");
            }

            stringBuilder.AppendLine("***").AppendLine();

            //stringBuilder.AppendLine().AppendLine("*** Weapons ****");
            //foreach (var keyPair in _weaponStatUsed)
            //{
            //    stringBuilder.AppendLine("***");
            //    var type = keyPair.Key;
            //    var used = _weaponStatUsed[type];
            //    var wins = _weaponStatWins[type];
            //    var perCents = (double)wins / (double)used * 100;
            //    stringBuilder.AppendLine($"{type.Name.ToUpper()}:");
            //    stringBuilder.AppendLine($"\tused: {used}\twins: {wins}\t%: {perCents}");

            //    stringBuilder.AppendLine("***");
            //}
            //stringBuilder.AppendLine("***").AppendLine();

            return stringBuilder.ToString();
        }

        private void InitStats()
        {
            _bot1WinsCount = 0;
            _bot2WinsCount = 0;
            _drawCount = 0;

            _weaponStatWins = new Dictionary<Type, int>();
            _weaponStatWins.Add(typeof(Dagger), 0);
            _weaponStatWins.Add(typeof(Bow), 0);
            _weaponStatWins.Add(typeof(Sword), 0);

            _weaponStatUsed = new Dictionary<Type, int>();
            _weaponStatUsed.Add(typeof(Dagger), 0);
            _weaponStatUsed.Add(typeof(Bow), 0);
            _weaponStatUsed.Add(typeof(Sword), 0);
        }

        private void AddStatFromBot(IBot bot)
        {
            var botWeaponType = bot.ReceivePlayer().Weapon.GetType();
            _weaponStatUsed[botWeaponType] = _weaponStatUsed[botWeaponType] + 1;
        }

        private void AddWinWeaponStat(IBot bot)
        {
            var botWeaponType = bot.ReceivePlayer().Weapon.GetType();
            _weaponStatWins[botWeaponType] = _weaponStatWins[botWeaponType] + 1;
        }
    }
}
