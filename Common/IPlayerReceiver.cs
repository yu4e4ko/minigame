﻿using MiniGame;

namespace Common
{
    public interface IPlayerReceiver
    {
        Player ReceivePlayer();

        void SendRoundResult(RoundResult roundResult);
    }
}
