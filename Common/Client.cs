﻿using System;
using System.Net.Sockets;
using MiniGame;

namespace Common
{
    public class Client : IDisposable
    {
        private TcpClient _tcpClient;
        private readonly int _port;

        public Client(int port)
        {
            _port = port;
        }

        public bool Connect(string ipAddress)
        {
            try
            {
                _tcpClient = new TcpClient();
                _tcpClient.Connect(ipAddress, _port);
                return true;
            }
            catch (Exception e)
            {
                throw new Exception($"Connection error! {e.Message}", e);
            }
        }

        public RoundResult SendPlayerInfo(Player player)
        {
            var playeerBytes = Helper.ObjectToByteArray(player);

            var stream = _tcpClient.GetStream();
            stream.Write(playeerBytes, 0, playeerBytes.Length);

            Console.WriteLine("Player sent");

            var responseMaxLenght = 2000;
            var response = new byte[responseMaxLenght];
            var responseLengh = stream.Read(response, 0, responseMaxLenght);

            var roundResult = Helper.ByteArrayToObject<RoundResult>(response, responseLengh);

            return roundResult;
        }

        public void Dispose()
        {
            if (_tcpClient != null)
            {
                _tcpClient.Close();
            }
        }
    }
}
