﻿using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Common
{
    public class Helper
    {
        public static byte[] ObjectToByteArray<T>(T someObject)
        {
            var binaryFormatter = new BinaryFormatter();
            using (var memoryStream = new MemoryStream())
            {
                binaryFormatter.Serialize(memoryStream, someObject);
                return memoryStream.ToArray();
            }

            throw new Exception("Error! Player -> byte array.");
        }

        public static T ByteArrayToObject<T>(byte[] bytes, int length)
        {
            var memoryStream = new MemoryStream();
            var binaryFormatter = new BinaryFormatter();

            T resultObject;

            try
            {
                memoryStream.Write(bytes, 0, length);
                memoryStream.Seek(0, SeekOrigin.Begin);

                resultObject = (T)binaryFormatter.Deserialize(memoryStream);
            }
            catch (Exception ex)
            {
                throw new Exception("Error! bytes -> PlayerResult. ", ex);
            }

            return resultObject;
        }
    }
}
