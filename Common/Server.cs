﻿using System;
using System.Net;
using System.Net.Sockets;
using MiniGame;

namespace Common
{
    public class Server : IDisposable, IPlayerReceiver
    {
        private Socket _soket;
        private TcpListener _tcpListener;
        private readonly int _port;

        public Server(int port)
        {
            _port = port;
        }

        public bool RunServer(string ipAddress)
        {
            try
            {
                IPAddress ipAd = IPAddress.Parse(ipAddress);
                _tcpListener = new TcpListener(ipAd, _port);

                _tcpListener.Start();
                _soket = _tcpListener.AcceptSocket();

                return true;
            }
            catch (Exception e)
            {
                throw new Exception($"Server error! {e.Message}", e);
            }
        }

        public Player ReceivePlayer()
        {
            var playerBytesMaxLength = 2000;
            var playerBytes = new byte[playerBytesMaxLength];

            var playerBytesLength = _soket.Receive(playerBytes);

            var player = Helper.ByteArrayToObject<Player>(playerBytes, playerBytesLength);
            return player;
        }

        public void SendRoundResult(RoundResult roundResult)
        {
            var roundResultBytes = Helper.ObjectToByteArray(roundResult);
            _soket.Send(roundResultBytes);
        }

        public void Dispose()
        {
            if (_soket != null)
            {
                _soket.Close();
            }

            if (_tcpListener != null)
            {
                _tcpListener.Stop();
            }
        }
    }
}
